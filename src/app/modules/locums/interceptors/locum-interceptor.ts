import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse} from '@angular/common/http';

import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

@Injectable()
export class LocumInterceptor implements HttpInterceptor {
  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(req)
      .pipe(map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && this.shouldModify(event)) {
          event = event.clone({body: this.modifyBody(event.body)});
        }
        return event;
      }));
  }

  private shouldModify(event) {
    const expectedUrl = /\/cl_filter\/latest_all$/.test(event.url);
    const expectedStatus = event.status === 200;
    return expectedStatus && expectedUrl;
  }

  private modifyBody(body: any) {
    const items = [];
    const ms = 1000;
    for (const index in body) {
      if (body.hasOwnProperty(index)) {
        const {unixTime_start, unixTime_end, time, ...args} = body[index];
        items.push({
          ...args,
          dateStart: new Date(unixTime_start * ms),
          dateFinish: new Date(unixTime_end * ms),
          dateAdded: new Date(time * ms),
          _index: +index
        });
      }
    }
    return items;
  }
}
