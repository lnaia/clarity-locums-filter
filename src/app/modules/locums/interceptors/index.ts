import {HTTP_INTERCEPTORS} from '@angular/common/http';

import {LocumInterceptor} from './locum-interceptor';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  {provide: HTTP_INTERCEPTORS, useClass: LocumInterceptor, multi: true}
];
