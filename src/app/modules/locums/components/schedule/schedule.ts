export class Schedule {
  dateStart: string;
  dateFinish: string;
  timeStart: string;
  timeFinish: string;
}
