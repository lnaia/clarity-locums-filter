import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Schedule} from './schedule';

@Component({
  selector: 'app-schedule',
  templateUrl: './schedule.component.html',
  styleUrls: ['./schedule.component.css'],
})
export class ScheduleComponent implements OnInit {
  @Output() scheduleUpdate = new EventEmitter<Schedule>();

  hide = true;
  schedule: Schedule = {
    dateStart: undefined,
    dateFinish: undefined,
    timeStart: undefined,
    timeFinish: undefined
  };

  constructor() {
  }

  ngOnInit() {
  }

  broadcast() {
    this.scheduleUpdate.emit(this.schedule);
  }
}
