import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Subject} from 'rxjs';
import {debounceTime} from 'rxjs/operators';

@Component({
  selector: 'app-search-area',
  templateUrl: './search-area.component.html',
  styleUrls: ['./search-area.component.css']
})
export class SearchAreaComponent implements OnInit {
  @Output() searchUpdate = new EventEmitter<string>();

  private debouncer: Subject<string> = new Subject<string>();
  search = '';
  hide = true;

  constructor() {
  }

  ngOnInit() {
    this.debouncer
      .pipe(debounceTime(800))
      .subscribe(v => this.searchUpdate.emit(v));
  }

  broadcast() {
    this.debouncer.next(this.search);
  }
}
