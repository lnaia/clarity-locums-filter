import { Component, OnInit } from '@angular/core';
import { Locum } from '../models/locum.model';
import { LocumService } from '../services/locum.service';
import { Schedule } from './schedule/schedule';

@Component({
  selector: 'app-locums',
  templateUrl: './locums.component.html',
  styleUrls: ['./locums.component.css']
})
export class LocumsComponent implements OnInit {
  locums: Locum[] = [];
  locumsAvailable: Locum[] = [];
  weekdaysFilter: number[];
  searchTerm = '';
  sorting = 'id';
  reverse = false;
  loading = false;
  hideFilters = true;
  scheduleFilter: Schedule = {
    dateStart: undefined,
    dateFinish: undefined,
    timeStart: undefined,
    timeFinish: undefined
  };

  constructor(
    private locumService: LocumService
  ) {
  }

  ngOnInit() {
    this.getLocums();
  }

  getLocums() {
    this.loading = true;
    this.locumService.getLocums()
      .subscribe(
        locums => {
          this.locums = locums;
          this.locumsAvailable = locums;
        },
        (err) => console.error(err),
        () => (this.loading = false));
  }

  getMapUrl(locum: Locum) {
    const base = 'https://www.google.ie/maps/search';
    return `${base}/${encodeURIComponent(locum.area)}+${encodeURIComponent(locum.area2)}/`;
  }

  filterWeekdays(weekdays: number[]) {
    this.weekdaysFilter = weekdays;
    this.applyFilters();
  }

  filterSchedule(schedule: Schedule) {
    this.scheduleFilter = schedule;
    this.applyFilters();
  }

  filterSearchTerm(searchTerm: string) {
    this.searchTerm = searchTerm;
  }

  applyFilters() {
    const locums = this.locumsAvailable;
    this.locums = locums.filter(({ dateFinish, dateStart }) => {
      const filters = [];

      if (Array.isArray(this.weekdaysFilter) && this.weekdaysFilter.length) {
        filters.push(this.weekdaysFilter.indexOf(dateStart.getDay()) !== -1);
      }

      if (this.scheduleFilter.dateStart) {
        const calculatedDate = this.strToDate(this.scheduleFilter.dateStart);
        filters.push(dateStart.getTime() >= calculatedDate.getTime());
      }

      if (this.scheduleFilter.dateFinish) {
        const calculatedDate = this.strToDate(this.scheduleFilter.dateFinish);
        filters.push(dateFinish.getTime() <= calculatedDate.getTime());
      }

      if (this.scheduleFilter.timeStart) {
        const calculatedDate = this.strToTime(dateFinish, this.scheduleFilter.timeStart);
        filters.push(dateStart.getTime() >= calculatedDate.getTime());
      }

      if (this.scheduleFilter.timeFinish) {
        const calculatedDate = this.strToTime(dateStart, this.scheduleFilter.timeFinish);
        filters.push(dateFinish.getTime() <= calculatedDate.getTime());
      }

      return filters.length > 0 ? filters.reduce((res, curr) => res && curr, true) : true;
    });
  }

  applySorting(type) {
    if (type === this.sorting) {
      this.reverse = !this.reverse;
    }

    this.sorting = type;
    const locums = this.locums.sort((a, b) => {
      let dateA, dateB;

      switch (type) {
        case 'day':
          return a.dateStart.getTime() - b.dateStart.getTime();

        case 'added':
          return a.dateAdded.getTime() - b.dateAdded.getTime();

        case 'startTime':
          dateA = this.normalizeDate(a.dateStart, a.dateStart);
          dateB = this.normalizeDate(b.dateStart, a.dateStart);
          return dateA.getTime() - dateB.getTime();

        case 'finishTime':
          dateA = this.normalizeDate(a.dateFinish, b.dateFinish);
          dateB = this.normalizeDate(b.dateFinish, b.dateFinish);
          return dateA.getTime() - dateB.getTime();

        default:
          return a[type] - b[type];
      }
    });

    this.locums = this.reverse ? locums.reverse() : locums;
  }

  strToDate(dateStr) {
    const matches = dateStr && dateStr.match(/(\d+)-(\d+)-(\d+)/);
    if (matches) {
      const [_, year, month, day] = matches;
      return new Date(year, month - 1, day);
    }
    return dateStr;
  }

  strToTime(rawDate: Date, filterTime: string): Date {
    const date = new Date(rawDate.getTime());
    const matches = filterTime.match(/(\d+):(\d+)/);
    if (matches) {
      const [_, hour, minute] = matches;
      date.setHours(+hour, +minute, 0);
    }
    return date;
  }

  // normalize hours in source for target
  normalizeDate(source: Date, target: Date): Date {
    const date = new Date(target.getTime());
    date.setHours(source.getHours(), source.getMinutes());
    return date;
  }
}
