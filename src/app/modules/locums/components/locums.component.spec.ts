import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { CoreModule } from '@app/core';
import { SharedModule } from '@app/shared';
import { LocumsComponent } from './locums.component';
import { ScheduleComponent } from './schedule/schedule.component';
import { SearchAreaComponent } from './search-area/search-area.component';
import { WeekdaysComponent } from './weekdays/weekdays.component';

describe('LocumsComponent', () => {
  let component: LocumsComponent;
  let fixture: ComponentFixture<LocumsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        LocumsComponent,
        WeekdaysComponent,
        ScheduleComponent,
        SearchAreaComponent
      ],
      imports: [
        CoreModule,
        SharedModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocumsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
