import {Component, EventEmitter, OnInit, Output} from '@angular/core';

const WEEKDAYS = [
  'monday', 'tuesday', 'wednesday', 'thursdays', 'friday', 'saturday', 'sunday'
];

const WEEKDAYS_MAP = {
  sunday: 0,
  monday: 1,
  tuesday: 2,
  wednesday: 3,
  thursdays: 4,
  friday: 5,
  saturday: 6
};

@Component({
  selector: 'app-weekdays',
  templateUrl: './weekdays.component.html',
  styleUrls: ['./weekdays.component.css'],
})
export class WeekdaysComponent implements OnInit {
  @Output() weekdaysUpdate = new EventEmitter<string[]>();

  weekdays: string[];
  selected: {};
  hide = true;

  constructor() {
    this.selected = {};
  }

  ngOnInit() {
    this.weekdays = WEEKDAYS;
  }

  broadcast() {
    const selectedWeekdays = [];
    Object.entries(this.selected).forEach(([key, value]) => {
      if (value) {
        selectedWeekdays.push(WEEKDAYS_MAP[key]);
      }
    });
    this.weekdaysUpdate.emit(selectedWeekdays);
  }
}
