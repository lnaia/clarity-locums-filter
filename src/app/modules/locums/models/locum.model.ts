export class Locum {
  id: number;
  area: string;
  area2: string;
  dateFinish: Date;
  dateStart: Date;
  dateAdded: Date;
  rate: string;
  system: string;
}
