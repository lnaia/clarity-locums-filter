import { NgModule } from '@angular/core';
import { CoreModule } from '../../core';
import { SharedModule } from '../../shared';
import { LocumsComponent } from './components/locums.component';
import { ScheduleComponent } from './components/schedule/schedule.component';
import { SearchAreaComponent } from './components/search-area/search-area.component';
import { WeekdaysComponent } from './components/weekdays/weekdays.component';
import { httpInterceptorProviders } from './interceptors';

@NgModule({
  imports: [
    CoreModule,
    SharedModule
  ],
  declarations: [
    LocumsComponent,
    WeekdaysComponent,
    ScheduleComponent,
    SearchAreaComponent
  ],
  providers: [
    httpInterceptorProviders
  ],
  exports: [
    LocumsComponent
  ]
})
export class LocumsModule {
}
