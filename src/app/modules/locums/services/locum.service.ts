import {Injectable} from '@angular/core';
import {Locum} from '../models/locum.model';
import {HttpClient} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {environment} from '../../../../environments/environment';


@Injectable({providedIn: 'root'})
export class LocumService {

  private locumUrl = `${environment.apiUrl}/cl_filter/latest_all`;

  constructor(
    private http: HttpClient
  ) {
  }

  getLocums(): Observable<Locum[]> {
    return this.http.get<Locum[]>(this.locumUrl)
      .pipe(
        catchError(this.handleError('getLocums', []))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(`${operation}: `, error);
      return of(result as T);
    };
  }
}
