import {TestBed, inject} from '@angular/core/testing';
import {SharedModule} from '../../../shared';
import {LocumService} from './locum.service';

describe('LocumService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        SharedModule
      ],
      providers: [
        LocumService
      ]
    });
  });

  it('should be created', inject([LocumService], (service: LocumService) => {
    expect(service).toBeTruthy();
  }));
});
