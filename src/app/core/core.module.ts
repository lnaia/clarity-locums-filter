import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SearchPipe } from './search.pipe';

@NgModule({
  declarations: [
    HeaderComponent,
    SearchPipe
  ],
  exports: [
    HeaderComponent,
    SearchPipe
  ]
})
export class CoreModule {
}
