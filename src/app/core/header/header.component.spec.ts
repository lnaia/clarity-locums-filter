import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HeaderComponent} from './header.component';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [HeaderComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render Locum Filter', async(() => {
    const fixtureLoc = TestBed.createComponent(HeaderComponent);
    fixtureLoc.detectChanges();
    const compiled = fixtureLoc.debugElement.nativeElement;
    expect(compiled.querySelector('a').textContent).toContain('Locum Filter');
  }));

  it('should render Home', async(() => {
    const fixtureLoc = TestBed.createComponent(HeaderComponent);
    fixtureLoc.detectChanges();
    const compiled = fixtureLoc.debugElement.nativeElement;
    expect(compiled.querySelector('ul li a').textContent).toContain('Home');
  }));
});
