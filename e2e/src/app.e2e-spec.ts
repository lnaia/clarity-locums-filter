import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should load a list of locums', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toMatch('locums available.');
  });
});
